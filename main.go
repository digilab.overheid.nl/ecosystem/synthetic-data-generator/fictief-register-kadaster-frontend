package main

import (
	"flag"
	"fmt"
	"log"
	"strings"
	"text/template"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster-frontend/backend"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster-frontend/helpers"
)

func main() {
	// Parse the optional port flag
	port := flag.Int("port", 80, "port on which to run the web server")
	flag.Parse()

	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"formatTime": helpers.FormatTime,
		"formatDate": helpers.FormatDate,
		"formatYear": helpers.FormatYear,
		"title":      cases.Title(language.Dutch).String, // Note: modern equivalent of the deperacted strings.Title function
		"trim":       strings.TrimSpace,
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// Buildings
	app.Get("/", func(c *fiber.Ctx) error {
		// Fetch the buildings from the backend
		var buildings []backend.Building
		if err := backend.Request("GET", "/buildings", nil, &buildings); err != nil {
			return fmt.Errorf("error fetching buildings: %v", err)
		}

		return c.Render("index", fiber.Map{"buildings": buildings})
	})

	buildings := app.Group("/buildings")

	buildings.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the building from the backend
		var building backend.Building
		if err := backend.Request("GET", fmt.Sprintf("/buildings/%s", c.Params("id")), nil, &building); err != nil {
			return fmt.Errorf("error fetching building: %v", err)
		}

		return c.Render("building-details", fiber.Map{"building": building})
	})

	// Plots
	plots := app.Group("/plots")

	plots.Get("/", func(c *fiber.Ctx) error {
		// Fetch the plots from the backend
		var plots []backend.Plot
		if err := backend.Request("GET", "/plots", nil, &plots); err != nil {
			return fmt.Errorf("error fetching plots: %v", err)
		}

		return c.Render("plot-index", fiber.Map{"plots": plots})
	})

	plots.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the plot from the backend
		var plot backend.Plot
		if err := backend.Request("GET", fmt.Sprintf("/plots/%s", c.Params("id")), nil, &plot); err != nil {
			return fmt.Errorf("error fetching plot: %v", err)
		}

		return c.Render("plot-details", fiber.Map{"plot": plot})
	})

	// People
	people := app.Group("/people")

	people.Get("/", func(c *fiber.Ctx) error {
		// Fetch the people from the backend
		var people []backend.Person
		if err := backend.Request("GET", "/people", nil, &people); err != nil {
			return fmt.Errorf("error fetching people: %v", err)
		}

		return c.Render("person-index", fiber.Map{"people": people})
	})

	people.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the person from the backend
		var person backend.Person
		if err := backend.Request("GET", fmt.Sprintf("/people/%s", c.Params("id")), nil, &person); err != nil {
			return fmt.Errorf("error fetching person: %v", err)
		}

		return c.Render("person-details", fiber.Map{
			"person": person,
		})
	})

	// Start server
	if err := app.Listen(fmt.Sprintf(":%d", *port)); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
