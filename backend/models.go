package backend

import (
	"time"

	"github.com/google/uuid"
)

// IMPROVE: import models from gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster/model

type Building struct {
	ID            uuid.UUID  `json:"id"`
	ConstructedAt *time.Time `json:"constructedAt,omitempty"`
	Purpose       string     `json:"purpose,omitempty"`
	Surface       int32      `json:"surface,omitempty"`
	Plots         []Plot     `json:"plots,omitempty"`
}

type Person struct {
	ID    uuid.UUID `json:"id"`
	Plots []Plot    `json:"plots,omitempty"`
}

type Municipality struct {
	Code string `json: code`
	Name string `json: name`
}

type Plot struct {
	ID           uuid.UUID    `json:"id"`
	Municipality Municipality `json:"municipality"`
	Section      string       `json:"section"`
	Number       int32        `json:"number"`
	Surface      int32        `json:"surface"`
	Buildings    []Building   `json:"buildings,omitempty"`
	Owners       []Person     `json:"owners,omitempty"`
}

type PlotPatch struct {
	ID           uuid.UUID     `json:"id"`
	Municipality *Municipality `json:"municipality"`
	Section      *string       `json:"section"`
	Number       *int32        `json:"number"`
	Surface      *int32        `json:"surface"`
}

type Reparcelled struct {
	From []uuid.UUID `json:"from"`
	To   []PlotPatch `json:"to"`
}

type BuildOnPlot struct {
	BuildingID uuid.UUID `json:"buildingID"`
	PlotID     uuid.UUID `json:"plotID"`
}
